#+TITLE: Deploy scripts for xuanzuanshi.com

- Vagrantfile: init a [[http://vagrantup.com/][vagrant]] box quickly for local development
- boostrap.sh: install docker in ubuntu
- docker-registry.sh: run private docker-registry service in local
- swapon.sh: turn on swap memory for vps with small RAM, less than 1024M.
- nginx-proxy.sh: the most important script. After you run ~docker-compose up
  -d~ in server(http://api.xuanzuanshi.com), web(http://www.xuanzuanshi.com),
  forum(http://forum.xuanzuanshi.com), you must run this script to make
  service exposed. Check this [[http://jasonwilder.com/blog/2014/03/25/automated-nginx-reverse-proxy-for-docker/][post]] for tech details.

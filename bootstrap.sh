#!/bin/bash

# Add the repository to your APT sources
sudo sh -c "echo deb https://apt.dockerproject.org/repo ubuntu-trusty main > /etc/apt/sources.list.d/docker.list"
# import the repository key
sudo apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

sudo apt-get -q -y update
sudo apt-get -q -y --force-yes install docker-engine git wget curl tmux

# install python pip
sudo apt-get -q -y --force-yes install python-pip

# install docker-compose
sudo pip install -U docker-compose

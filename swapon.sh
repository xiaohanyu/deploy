#!/bin/bash
# run it with sudo
# https://www.digitalocean.com/community/tutorials/how-to-add-swap-on-ubuntu-14-04

SWAPFILE=/swapfile
SWAPSIZE=$1

ls $SWAPFILE && swapoff $SWAPFILE && rm $SWAPFILE
fallocate -l $SWAPSIZE $SWAPFILE
chmod 600 $SWAPFILE
mkswap $SWAPFILE
swapon $SWAPFILE
grep "swapfile" /etc/fstab || echo "$SWAPFILE   none    swap    sw    0   0" >> /etc/fstab
